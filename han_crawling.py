from urllib.request import urlopen
from bs4 import BeautifulSoup
from urllib import parse
import csv
import re

def get_html(url):
    html = urlopen(url)
    source = html.read()
    html.close()
    return source

start = "2017.05.01"
end = "2019.08.04"
keyword = "미투운동"

f = open('output.csv', 'w', encoding='utf-8', newline='')
wr = csv.writer(f)

pageidx = 0
try:
    while True:
        url = f"http://search.hani.co.kr/Search?command=query&keyword={parse.quote(keyword)}&media=news&submedia=&sort=d&period=all&datefrom={start}&dateto={end}&pageseq={pageidx}"
        source  = get_html(url)
        pageidx = pageidx + 1

        soup = BeautifulSoup(source, "html5lib")
        articles = soup.find_all("dt")
        if articles.__len__() < 1:
            print("END!")
            break
        for title in articles:
            atag = title.find("a")
            if atag is not None:
                arti_title = title.text
                arti_url = atag["href"]
                arti_date = title.parent.findNext(class_="date").text.strip()

                source = get_html(atag["href"])
                soup = BeautifulSoup(source, "html5lib")
                articles = soup.find("div", class_="text")
                arti_contents = re.sub('\t|,|\r|\n', '', articles.text.strip())
                row = [arti_title, arti_date,arti_contents ]
                wr.writerow(row)
                print(row)
finally:
    f.close()

            